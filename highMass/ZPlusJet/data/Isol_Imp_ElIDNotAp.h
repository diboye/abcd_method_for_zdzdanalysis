//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Apr  9 10:05:27 2018 by ROOT version 6.04/16
// from TTree myTree/myTree
// found on file: ZplusJets_Imp_Iso_elIDNoAp.root
//////////////////////////////////////////////////////////

#ifndef Isol_Imp_ElIDNotAp_h
#define Isol_Imp_ElIDNotAp_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>

// Header file for the classes stored in the TTree if any.
#include "TLorentzVector.h"

class Isol_Imp_ElIDNotAp : public TSelector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
  TFile* fOut;


  TH2D* hIsol_vs_Impact_lA_4e;
  TH2D* hIsol_vs_Impact_lB_4e;
  TH2D* hIsol_vs_Impact_lC_4e;
  TH2D* hIsol_vs_Impact_lD_4e;
  TH1F* hCuts_4e;
  TH1F* hCuts_NW_4e;

  TH2D* hIsol_vs_Impact_lA_2e2m;
  TH2D* hIsol_vs_Impact_lB_2e2m;
  TH2D* hIsol_vs_Impact_lC_2e2m;
  TH2D* hIsol_vs_Impact_lD_2e2m;
  TH1F* hCuts_2e2m;
  TH1F* hCuts_NW_2e2m;

  TH2D* hIsol_vs_Impact_lA_4m;
  TH2D* hIsol_vs_Impact_lB_4m;
  TH2D* hIsol_vs_Impact_lC_4m;
  TH2D* hIsol_vs_Impact_lD_4m;
  TH1F* hCuts_4m;
  TH1F* hCuts_NW_4m;
     TH1F* hCutflow_4e;
   TH1F* hCutflow_2e2m;
   TH1F* hCutflow_4m;
  



   TLorentzVector  *lep12;
   TLorentzVector  *lep34;
   TLorentzVector  *lep32;
   TLorentzVector  *lep14;
   TLorentzVector  *lep1234;
   TLorentzVector  *lep1;
   TLorentzVector  *lep2;
   TLorentzVector  *lep3;
   TLorentzVector  *lep4;
   Int_t           IdLep1;
   Int_t           IdLep2;
   Int_t           IdLep3;
   Int_t           IdLep4;
   Float_t         d0SigLep1;
   Float_t         d0SigLep2;
   Float_t         d0SigLep3;
   Float_t         d0SigLep4;
   Int_t           elIDLep1;
   Int_t           elIDLep2;
   Int_t           elIDLep3;
   Int_t           elIDLep4;
   Int_t           muID4Lep;
   Double_t        EvtWeight;
   Int_t           llll_pdgIdSum;
   ULong64_t       eventNumber;
   Int_t           RunNumber;
   Int_t           l_isIsolFixedCutLoose;
   Int_t           mc_channel_number;
   Double_t        max_el_d0Sig;
   Double_t        max_mu_d0Sig;
   Double_t        events_all;
 
   
   Bool_t d0SigB_allpass ;
   Bool_t d0SigB_34Npass ;
   Bool_t d0SigB_3pass_4Npass ;
   Bool_t d0SigB_3Npass_4pass;
    Bool_t lepton_d0Sig[4];
    Bool_t Q_Veto;
    Bool_t Electron_ID;
   Bool_t LowMassVeto;
   Bool_t HWinHM;
   Bool_t ZVeto;
   Bool_t LooseSR_HM;
   Bool_t MediumSR;
   Bool_t ZVR1HM;
   Bool_t ZVR2HM;

    Double_t myFunc(Double_t q)
   {
     // local variable declaration
     Double_t result;
     Double_t h = 3.72817e+00; 
     Double_t Y = 5.16317e+01; 
     Double_t S = 1.66099e+01 ; 
     Double_t B1 = -2.61777e+00 ; 
     Double_t B2 =  -2.66119e-02; 
     Double_t T = 6.39341e+00;
   
     
     if (q < Y-T)
       {
	 result = B1+B2*(q-Y)+h*exp(T*(2*q-2*Y+T)/2*pow(S,2));
       }
		   
     else if (q > Y-T) 
       result = B1+B2*(q-Y)+h*exp(-pow((q-Y),2)/2*pow(S,2));
     
     return result; 
   }

   // List of branches
   TBranch        *b_lep12;   //!
   TBranch        *b_lep34;   //!
   TBranch        *b_lep32;   //!
   TBranch        *b_lep14;   //!
   TBranch        *b_lep1234;   //!
   TBranch        *b_lep1;   //!
   TBranch        *b_lep2;   //!
   TBranch        *b_lep3;   //!
   TBranch        *b_lep4;   //!
   TBranch        *b_IdLep1;   //!
   TBranch        *b_IdLep2;   //!
   TBranch        *b_IdLep3;   //!
   TBranch        *b_IdLep4;   //!
   TBranch        *b_d0SigLep1;   //!
   TBranch        *b_d0SigLep2;   //!
   TBranch        *b_d0SigLep3;   //!
   TBranch        *b_d0SigLep4;   //!
   TBranch        *b_elIDLep1;   //!
   TBranch        *b_elIDLep2;   //!
   TBranch        *b_elIDLep3;   //!
   TBranch        *b_elIDLep4;   //!
   TBranch        *b_muID4Lep;   //!
   TBranch        *b_EvtWeight;   //!
   TBranch        *b_llll_pdgIdSum;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_l_isIsolFixedCutLoose;   //!
   TBranch        *b_mc_channel_number;   //!
   TBranch        *b_max_el_d0Sig;   //!
   TBranch        *b_max_mu_d0Sig;   //!
   TBranch        *b_events_all;   //!
   TBranch        *b_Q_Veto;
   TBranch        *b_Electron_ID;
   TBranch        *b_LowMassVeto;
   TBranch        *b_HWinHM;
   TBranch        *b_ZVeto;
   TBranch        *b_LooseSR_HM;
   TBranch        *b_MediumSR;
   TBranch        *b_ZVR1HM;
   TBranch        *b_ZVR2HM;

   Isol_Imp_ElIDNotAp(TTree * /*tree*/ =0) : fChain(0) { }
   virtual ~Isol_Imp_ElIDNotAp() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(Isol_Imp_ElIDNotAp,0);
};

#endif

#ifdef Isol_Imp_ElIDNotAp_cxx
void Isol_Imp_ElIDNotAp::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   lep12 = 0;
   lep34 = 0;
   lep32 = 0;
   lep14 = 0;
   lep1234 = 0;
   lep1 = 0;
   lep2 = 0;
   lep3 = 0;
   lep4 = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("lep12", &lep12, &b_lep12);
   fChain->SetBranchAddress("lep34", &lep34, &b_lep34);
   fChain->SetBranchAddress("lep32", &lep32, &b_lep32);
   fChain->SetBranchAddress("lep14", &lep14, &b_lep14);
   fChain->SetBranchAddress("lep1234", &lep1234, &b_lep1234);
   fChain->SetBranchAddress("lep1", &lep1, &b_lep1);
   fChain->SetBranchAddress("lep2", &lep2, &b_lep2);
   fChain->SetBranchAddress("lep3", &lep3, &b_lep3);
   fChain->SetBranchAddress("lep4", &lep4, &b_lep4);
   fChain->SetBranchAddress("IdLep1", &IdLep1, &b_IdLep1);
   fChain->SetBranchAddress("IdLep2", &IdLep2, &b_IdLep2);
   fChain->SetBranchAddress("IdLep3", &IdLep3, &b_IdLep3);
   fChain->SetBranchAddress("IdLep4", &IdLep4, &b_IdLep4);
   fChain->SetBranchAddress("d0SigLep1", &d0SigLep1, &b_d0SigLep1);
   fChain->SetBranchAddress("d0SigLep2", &d0SigLep2, &b_d0SigLep2);
   fChain->SetBranchAddress("d0SigLep3", &d0SigLep3, &b_d0SigLep3);
   fChain->SetBranchAddress("d0SigLep4", &d0SigLep4, &b_d0SigLep4);
   fChain->SetBranchAddress("elIDLep1", &elIDLep1, &b_elIDLep1);
   fChain->SetBranchAddress("elIDLep2", &elIDLep2, &b_elIDLep2);
   fChain->SetBranchAddress("elIDLep3", &elIDLep3, &b_elIDLep3);
   fChain->SetBranchAddress("elIDLep4", &elIDLep4, &b_elIDLep4);
   fChain->SetBranchAddress("muID4Lep", &muID4Lep, &b_muID4Lep);
   fChain->SetBranchAddress("EvtWeight", &EvtWeight, &b_EvtWeight);
   fChain->SetBranchAddress("llll_pdgIdSum", &llll_pdgIdSum, &b_llll_pdgIdSum);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("l_isIsolFixedCutLoose", &l_isIsolFixedCutLoose, &b_l_isIsolFixedCutLoose);
   // fChain->SetBranchAddress("mc_channel_number", &mc_channel_number, &b_mc_channel_number);
   fChain->SetBranchAddress("max_el_d0Sig", &max_el_d0Sig, &b_max_el_d0Sig);
   fChain->SetBranchAddress("max_mu_d0Sig", &max_mu_d0Sig, &b_max_mu_d0Sig);
   // fChain->SetBranchAddress("events_all", &events_all, &b_events_all);
   fChain->SetBranchAddress("Q_Veto", &Q_Veto, &b_Q_Veto);
   fChain->SetBranchAddress("Electron_ID", &Electron_ID, &b_Electron_ID);
   fChain->SetBranchAddress("LowMassVeto", &LowMassVeto, &b_LowMassVeto);
   fChain->SetBranchAddress("HWinHM", &HWinHM, &b_HWinHM);
   fChain->SetBranchAddress("ZVeto", &ZVeto, &b_ZVeto);
   fChain->SetBranchAddress("LooseSR_HM", &LooseSR_HM, &b_LooseSR_HM);
   fChain->SetBranchAddress("MediumSR", &MediumSR, &b_MediumSR);
   fChain->SetBranchAddress("ZVR1HM", &ZVR1HM, &b_ZVR1HM);
   fChain->SetBranchAddress("ZVR2HM", &ZVR2HM, &b_ZVR2HM);
}

Bool_t Isol_Imp_ElIDNotAp::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

#endif // #ifdef Isol_Imp_ElIDNotAp_cxx
