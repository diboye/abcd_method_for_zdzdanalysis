//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed May  2 10:12:16 2018 by ROOT version 5.34/36
// from TTree myTree/myTree
// found on file: ggZZ4lNoSherpa_20180131_mc16a_Iso_Imp_ElID_NoAp.root
//////////////////////////////////////////////////////////

#ifndef abcd_method_h
#define abcd_method_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>

// Header file for the classes stored in the TTree if any.
#include <TLorentzVector.h>

// Fixed size dimensions of array or collections stored in the TTree if any.

class abcd_method : public TSelector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain

   // Declaration of leaf types
   TFile* fOut;
   //Int_t i, j;
   TH1D*hist[3][75];
   double lumi = 36.2077e06; 
   
   

   // Double_t  Events_all_345709;
   TString datasetID;
   TString channel;
   
   TLorentzVector  *lep12;
   TLorentzVector  *lep34;
   TLorentzVector  *lep32;
   TLorentzVector  *lep14;
   TLorentzVector  *lep1234;
   TLorentzVector  *lep1;
   TLorentzVector  *lep2;
   TLorentzVector  *lep3;
   TLorentzVector  *lep4;
   Int_t           IdLep1;
   Int_t           IdLep2;
   Int_t           IdLep3;
   Int_t           IdLep4;
   Float_t         d0SigLep1;
   Float_t         d0SigLep2;
   Float_t         d0SigLep3;
   Float_t         d0SigLep4;
   Int_t           elIDLep1;
   Int_t           elIDLep2;
   Int_t           elIDLep3;
   Int_t           elIDLep4;
   Int_t           muID4Lep;
   Double_t        EvtWeight;
   Int_t           llll_pdgIdSum;
   ULong64_t       eventNumber;
   Int_t           RunNumber;
   Int_t           l_isIsolFixedCutLoose;
   Int_t           MC_channel_number;
   Double_t        max_el_d0Sig;
   Double_t        max_mu_d0Sig;
   Double_t        Events_all;
   Bool_t lepton_d0Sig[4];
   Bool_t d0SigB_allpass;
   Bool_t d0SigB_34Npass;
   Bool_t d0SigB_3pass_4Npass;
   Bool_t d0SigB_3Npass_4pass;
    Bool_t Q_Veto;

   // List of branches
   TBranch        *b_lep12;   //!
   TBranch        *b_lep34;   //!
   TBranch        *b_lep32;   //!
   TBranch        *b_lep14;   //!
   TBranch        *b_lep1234;   //!
   TBranch        *b_lep1;   //!
   TBranch        *b_lep2;   //!
   TBranch        *b_lep3;   //!
   TBranch        *b_lep4;   //!
   TBranch        *b_IdLep1;   //!
   TBranch        *b_IdLep2;   //!
   TBranch        *b_IdLep3;   //!
   TBranch        *b_IdLep4;   //!
   TBranch        *b_d0SigLep1;   //!
   TBranch        *b_d0SigLep2;   //!
   TBranch        *b_d0SigLep3;   //!
   TBranch        *b_d0SigLep4;   //!
   TBranch        *b_elIDLep1;   //!
   TBranch        *b_elIDLep2;   //!
   TBranch        *b_elIDLep3;   //!
   TBranch        *b_elIDLep4;   //!
   TBranch        *b_muID4Lep;   //!
   TBranch        *b_EvtWeight;   //!
   TBranch        *b_llll_pdgIdSum;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_l_isIsolFixedCutLoose;   //!
   TBranch        *b_MC_channel_number;   //!
   TBranch        *b_max_el_d0Sig;   //!
   TBranch        *b_max_mu_d0Sig;   //!
   TBranch        *b_Events_all;   //!
   TBranch        *b_Q_Veto;
   TBranch        *b_LowMassVeto;
   Bool_t LowMassVeto;

   abcd_method(TTree * /*tree*/ =0) : fChain(0) { }
   virtual ~abcd_method() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(abcd_method,0);
};

#endif

#ifdef abcd_method_cxx
void abcd_method::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   lep12 = 0;
   lep34 = 0;
   lep32 = 0;
   lep14 = 0;
   lep1234 = 0;
   lep1 = 0;
   lep2 = 0;
   lep3 = 0;
   lep4 = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("lep12", &lep12, &b_lep12);
   fChain->SetBranchAddress("lep34", &lep34, &b_lep34);
   fChain->SetBranchAddress("lep32", &lep32, &b_lep32);
   fChain->SetBranchAddress("lep14", &lep14, &b_lep14);
   fChain->SetBranchAddress("lep1234", &lep1234, &b_lep1234);
   fChain->SetBranchAddress("lep1", &lep1, &b_lep1);
   fChain->SetBranchAddress("lep2", &lep2, &b_lep2);
   fChain->SetBranchAddress("lep3", &lep3, &b_lep3);
   fChain->SetBranchAddress("lep4", &lep4, &b_lep4);
   fChain->SetBranchAddress("IdLep1", &IdLep1, &b_IdLep1);
   fChain->SetBranchAddress("IdLep2", &IdLep2, &b_IdLep2);
   fChain->SetBranchAddress("IdLep3", &IdLep3, &b_IdLep3);
   fChain->SetBranchAddress("IdLep4", &IdLep4, &b_IdLep4);
   fChain->SetBranchAddress("d0SigLep1", &d0SigLep1, &b_d0SigLep1);
   fChain->SetBranchAddress("d0SigLep2", &d0SigLep2, &b_d0SigLep2);
   fChain->SetBranchAddress("d0SigLep3", &d0SigLep3, &b_d0SigLep3);
   fChain->SetBranchAddress("d0SigLep4", &d0SigLep4, &b_d0SigLep4);
   fChain->SetBranchAddress("elIDLep1", &elIDLep1, &b_elIDLep1);
   fChain->SetBranchAddress("elIDLep2", &elIDLep2, &b_elIDLep2);
   fChain->SetBranchAddress("elIDLep3", &elIDLep3, &b_elIDLep3);
   fChain->SetBranchAddress("elIDLep4", &elIDLep4, &b_elIDLep4);
   fChain->SetBranchAddress("muID4Lep", &muID4Lep, &b_muID4Lep);
   fChain->SetBranchAddress("EvtWeight", &EvtWeight, &b_EvtWeight);
   fChain->SetBranchAddress("llll_pdgIdSum", &llll_pdgIdSum, &b_llll_pdgIdSum);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("l_isIsolFixedCutLoose", &l_isIsolFixedCutLoose, &b_l_isIsolFixedCutLoose);
   fChain->SetBranchAddress("MC_channel_number", &MC_channel_number, &b_MC_channel_number);
   fChain->SetBranchAddress("max_el_d0Sig", &max_el_d0Sig, &b_max_el_d0Sig);
   fChain->SetBranchAddress("max_mu_d0Sig", &max_mu_d0Sig, &b_max_mu_d0Sig);
   fChain->SetBranchAddress("Events_all", &Events_all, &b_Events_all);
   fChain->SetBranchAddress("Q_Veto", &Q_Veto, &b_Q_Veto);
   fChain->SetBranchAddress("LowMassVeto", &LowMassVeto, &b_LowMassVeto);
}

Bool_t abcd_method::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;

   /* Dataset groupings:
gg#rightarrowZZ*#rightarrow4l (sherpa no H) : mc_channel_number==345709 
q#bar{q}#rightarrowZZ*#rightarrow4l (sherpa) : (mc_channel_number==364250&&killEvent==1)||mc_channel_number==364251||mc_channel_number==364252 
gg#rightarrowH#rightarrow4l (powheg) : mc_channel_number==345060 
VVV/VBS #rightarrow 4l+2X : (mc_channel_number==364243||mc_channel_number==364245||mc_channel_number==364247||mc_channel_number==364248)||(mc_channel_number==364283) 
Z+(t#bar{t}/J/#Psi/#Upsilon) : (mc_channel_number==410069||mc_channel_number==410070)||(mc_channel_number>=304697&&mc_channel_number<=304704) 
WZ (powheg) : mc_channel_number==361601 
Z + jets (powheg) : mc_channel_number==361106||mc_channel_number==361107||mc_channel_number==361108 
t#bar{t} : mc_channel_number==410501*/
}

#endif // #ifdef abcd_method_cxx
